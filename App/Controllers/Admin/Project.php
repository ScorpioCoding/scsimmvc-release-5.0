<?php
namespace App\Controllers\Admin;
use App\Core\Controller;
use App\Core\View;
/**
 *  Project
 */
class Project extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	protected function before()
	{
		//Check if Logged in is true
		Session::init();
		$auth = Session::get('auth');
		if ($auth == false) {
			Session::destroy();
			header('Location: ../../Home/index');
			return false;
		}
		else 
			return true;
	}

	public function indexAction()
	{
		View::render('b1', 'project');
	}
    protected function after()
	{
		
	}

} //END CLASS
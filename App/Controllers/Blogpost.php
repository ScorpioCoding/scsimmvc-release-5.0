<?php
namespace App\Controllers;
use App\Core\Controller;
use App\Core\View;
/**
 *  Blogpost
 */
class Blogpost extends Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	protected function before()
	{

	}

	public function indexAction( $args = array() )
	{
		$scVariables = self::getScVariables();
		$scVariables['scTitle'] = $args['title'];

		//input
		$input = array( 'id' => $args['id'], 'title' => $args['title'], 'name' => 'kribo');

		$data = array_merge($scVariables, $input);

		// View::render('f1', 'blogPost' , $data);

		View::renderTemplate('blogPost.html', $data);



	}

    protected function after()
	{
		
	}

}

<?php
namespace App\Controllers;
use App\Core\Controller;
use App\Core\View;
/**
 *  Contact
 */
class Contact extends Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	protected function before()
	{

	}

	public function indexAction()
	{
		View::render('f1', 'contact');
	}
	  
    protected function after()
	{
		
	}

}
// END CLASS
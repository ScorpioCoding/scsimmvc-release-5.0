<?php
namespace App\Controllers;

use App\Core\Controller;
use App\Core\View;
use App\Models\mLogin;
/**
 *  Login
 */
class Login extends Controller
{
	
	public function __construct()
	{
		parent::__construct();
	}

	protected function before()
	{

	}

	public function indexAction()
	{
		View::render('f0', 'login');
	}	

    protected function after()
	{

	}
	
	public function runAction()
	{	
		mLogin::run();
	}


} //END CLASS
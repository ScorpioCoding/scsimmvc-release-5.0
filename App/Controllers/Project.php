<?php
namespace App\Controllers;
use App\Core\Controller;
use App\Core\View;
/**
 *  Project
 */
class Project extends Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	protected function before()
	{

	}

	public function indexAction()
	{
			View::render('f1', 'project');
	}

    protected function after()
	{
		
	}


} //END CLASS
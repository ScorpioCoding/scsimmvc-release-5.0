<?php
namespace App\Core;

use App\Core\Router;

// ------------------------------ THE WRAPPER -------------------------------------------

/**
 *       remember $this->  refers to its self the class App
 *       so does self::   and    App::   they to references to itself
 */
class App extends Functions
{
    public function __construct()
    {
        parent::__construct();
        self::init();
		self::autoload(); 
        self::router();
    }

    private static function init()
    {


        //      Load configuration file
        $GLOBALS['config'] = include PATH_CONFIG . "config.php";

        //      Load core classes
        require PATH_CORE . "Router.php";
        require PATH_CORE . "Database.php";
        require PATH_CORE . "Controller.php";
        require PATH_CORE . "Model.php";
        require PATH_CORE . "View.php";
        require PATH_CORE . "Session.php";
       
        //    session start
        //session_start();

    }

    private static function autoload()
    {
        //AUTOLOADER WITH CUSTOM LOAD METHOD


        spl_autoload_register(function ($class)
        {
            $fileName  = '';
            $namespace = '';
            $parentDir = '';

            $class = ltrim($class, '\\');
            $lastNsPos = strrpos($class, '\\');
            $namespace = substr($class, 0, $lastNsPos);
            $class = substr($class, $lastNsPos + 1);

            $lastNsPos = strrpos($namespace, '\\');
            $parentDir = substr($namespace, $lastNsPos + 1);

            $fileName  = str_replace("\\", "/", $namespace) .'/';
            $fileName .= str_replace('_', DS, $class) . '.php';

            //echo 'class : '.$class.'<br> namespace : '.$namespace.'<br> filename : '.$fileName.'<br>';

            switch ($parentDir)
            {
                case 'Controllers':
                    $file = PATH_CONTROLLER . $class . ".php";
                    if ( is_readable( $file ) ) require $file;
                    break;
                case 'Admin':
                    $file = PATH_CONTROLLER_ADMIN . $class . ".php";
                    if ( is_readable( $file ) ) require $file;
                    break;
                case 'Dev' :
                    $file = PATH_CONTROLLER_DEV . $class . ".php";
                    if ( is_readable( $file ) ) require $file;
                    break;
                case 'Libs' :
                     $file = PATH_LIBS . $class . ".php";
                    if ( is_readable( $file ) ) require $file;
                    break;
                default:
                     $file = PATH_MODELS . $class . ".php";
                    if ( is_readable( $file ) ) require $file;                   
                    break;
            }

        });

    }



    private static function router()
    {   
        //ROUTING
        $router = new Router();

        //ADD DEFAULT ROUTES
        $router->add( '', ['controller' => 'Home' , 'action' => 'index'] );
        $router->add( '{controller}/{action}' );
        //$router->add( '{controller}/{action}/{id:\d+}');
        //$router->add( '{controller}/{action}/{id:\d+}/{title}' );
        $router->add( 'Blogpost/index/{id:\d+}/{title}' , [ 'controller' => 'Blogpost', 'action' => 'index' ] );
        $router->add( 'admin/{controller}/{action}' , ['namespace' => 'Admin'] );
        $router->add( 'dev/{controller}/{action}' , ['namespace' => 'Dev'] );




        //PARSING URL
        $tokens = htmlspecialchars($_GET['url']);

        // if ($router->match($tokens)) {
        //     echo '<pre>';
        //     var_dump($router->getParams());
        //     echo '</pre>';
        // } else {
        //     echo "No route found for URL '$tokens'";
        // }

        //DISPATCH 
        $router->dispatch($tokens);

    }



} //END CLASS

?>
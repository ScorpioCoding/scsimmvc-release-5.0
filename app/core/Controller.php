<?php
namespace App\Core;

/**
*  Base Controller
*/
class Controller
{

	protected $route_params = [];

	public function __construct( $route_params = array() )
    {
		//parent::__construct();
		$this->route_params = $route_params;
	}
	public function getRouteParams()
	{
		return $this->route_params;
	}

	/**
     * Magic method called when a non-existent or inaccessible method is
     * called on an object of this class. Used to execute before and after
     * filter methods on action methods. Action methods need to be named
     * with an "Action" suffix, e.g. indexAction, showAction etc.
     *
     * @param string $name  Method name
     * @param array $args Arguments passed to the method
     *
     * @return void
     */
    public function __call($name, $args)
    {
        $method = $name . 'Action';

        if (method_exists($this, $method)) {
            if ($this->before() !== false) {
                call_user_func_array([$this, $method], $args);
                $this->after();
            }
        } else {
			 throw new \Exception("Controller.php : Method $method not found in controller : ". get_class($this) );
        }
    }

	/**
     * Before filter - called before an action method.
     *
     * @return void
     */
    protected function before() {}

    /**
     * After filter - called after an action method.
     *
     * @return void
     */
    protected function after() {}


	
	protected function getScVariables()
	{
		$file = PATH_LIBS .'scVariables.php';
		if ( is_readable( $file ) )
		{ 
			require($file);
			return $scVariables;
		}
		else
			throw new Exception("Controller.php : getScVariables : File doesnt exist : $file");
	}

	protected function translation($translation)
	{
		//a method to acces / bind the translation to a view/ Model variables
		//eg.    in the template header the title
		// trans.en.php  en  - title = english
		// trans.nl.php  nl  - title = nederlands

		//these translation files can be outsourced

		//these translations are for our core elements and not dynamic centent...
		//like buttons, links, headers, fixed content,

		// the translation array is then sent to the view where the arry is extracted into seperate variables
		// which can be found in the tempplates.

		// $file = PATH_MODEL . $model .'.php';
		// if ( file_exist( $file ) )
		// { 
		// 	require($file);
		// 	return new $model();
		// }		
	}

}//END CLASS
?>
